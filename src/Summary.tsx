import {useLocation, useNavigate} from "react-router-dom";
import React from "react";
import moment from "moment";

export default function Summary() {
    const salaryBreakdown: any = useLocation().state;
    const navigate = useNavigate();
    return (
        <div className="App-header">
            <h2>Podsumowanie Pensji:</h2>
            <table className="SummaryTable">
                <tr>
                    <th>Miesiąc</th>
                    <th>Pensja brutto</th>
                    <th>ZUS</th>
                    <th>Ubezpieczenie zdrowotne</th>
                    <th>Zaliczka na podatek</th>
                    <th>Pensja netto:</th>
                </tr>
                {salaryBreakdown && salaryBreakdown.map((item: any, index:number) => (
                    <tr>
                        <td>{moment.months(index)}</td>
                        <td>{item.grossSalary}</td>
                        <td>{item.socialInsuranceContribution.socialInsuranceContributionValue}</td>
                        <td>{item.healthInsurance}</td>
                        <td>{item.taxPrepayment}</td>
                        <td>{item.netSalary}</td>
                    </tr>
                ))}
            </table>
            <button onClick={() => navigate("/paymentcalculator")}>powróć</button>

        </div>
    );
}