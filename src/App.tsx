import React, {ChangeEventHandler, useEffect, useState} from 'react';
import './App.css';
import axios from 'axios'
import {useNavigate} from "react-router-dom";
import moment from "moment"
import 'moment/locale/pl';

const Checkbox = ({
                      label,
                      value,
                      onChange
                  }: { label: string; value: boolean; onChange: ChangeEventHandler }) => {
    return (
        <label>
            <input type="checkbox" checked={value} onChange={onChange}/>
            {label}
        </label>
    );
};

function App() {
    useEffect(() => {
        moment.locale(['pl']);
    }, [])
    const months = moment.months();
    const navigate = useNavigate();
    const [salary, setSalary] = useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    const [over26, setOver26] = useState(true)
    const [isWorkingInPlaceOfResidence, setIsWorkingInPlaceOfResidence] = useState(true)
    const [taxReliefEnabled, setTaxReliefEnabled] = useState(true)
    const [fixedSalary, setFixedSalary] = useState(true)
    const calculate = () => {
        const calculatedSalary = fixedSalary ? salary.map(() => salary[0]) : salary
        axios.get('https://czwcx55mpwrbdytghps6kxosq40nlmvj.lambda-url.eu-central-1.on.aws/', {
            params: {
                employee: {
                    salary: calculatedSalary,
                    over26,
                    isWorkingInPlaceOfResidence,
                    taxReliefEnabled
                }
            }
        }).then(response => {
            console.log(response.data)
            navigate("/summary", {state: response.data, replace: true})
        }).catch((error) => {
            console.log(error)
        })
    }
    const updateSalary = (value: number, i: number) => {
        const newSalary = salary.slice()
        newSalary[i] = value;
        setSalary(newSalary);
    }
    return (
        <div className="App">
            <header className="App-header">
                <Checkbox label={"Mam skończone 26 lat"} value={over26} onChange={() => setOver26(!over26)}/>
                <Checkbox label={"Pracuje w miejscu zamieszkania"} value={isWorkingInPlaceOfResidence}
                          onChange={() => setIsWorkingInPlaceOfResidence(!isWorkingInPlaceOfResidence)}/>
                <Checkbox label={"Korzystam z ulgi dla klasy średniej"} value={taxReliefEnabled}
                          onChange={() => setTaxReliefEnabled(!taxReliefEnabled)}/>
                <Checkbox label={"Mam stałą pensję"} value={fixedSalary}
                          onChange={() => setFixedSalary(!fixedSalary)}/>
                {fixedSalary ? <input type="number" value={salary[0]}
                                      onChange={(e) => {
                                          updateSalary(parseInt(e.currentTarget.value), 0)
                                      }}/> :
                    months.map((item, index) => (
                        <div>{item} <input type="number" value={salary[index]}
                                           onChange={(e) => {
                                               updateSalary(parseInt(e.currentTarget.value), index)
                                           }}/></div>
                    ))}
                <button onClick={calculate}>Oblicz</button>
            </header>
        </div>
    );
}

export default App;
