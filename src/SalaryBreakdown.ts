export default interface SalaryBreakdown {
    grossSalary: number;
    SocialInsuranceContribution: {
        pensionContribution: number,
        disabilityPensionContribution: number,
        sicknessContribution: number,
    }
    healthInsurance: number;
    taxDeductibleCost: number;
    taxPrepayment: number;
    netSalary: number;
}
